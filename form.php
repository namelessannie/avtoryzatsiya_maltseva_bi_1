<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Авторизация	</title>
	<style type="text/css">
		.container {
		display: inline-block;
		background: #ccaead;
		width: 40%;
		padding: 90px 0px 20px 90px;	
		border-radius: 20px;
		box-shadow: 5px 5px 5px #584b59;
		margin: 100px 0px 0px 400px;
	}
		
	input {
  		width: 80%;
  		height: 42px;
 		padding-left: 10px;
  		border: 2px solid #20104d;
 		border-radius: 5px;
 		outline: none;
 		margin: 5px;
 		background: #F9F0DA;
 		color: #000000;
	}

 	input[type=submit] {
 		display: block;
		margin: 20px 20px 0px 140px;
       	width: 40%;
       	cursor: pointer; 
		font-size: 20px;  
		text-decoration: none; 
		padding: 10px 32px; 
		color: #ffffff; 
		background-color: #581763; 
		border-radius: 18px; 
		border: 2px solid #471247;
    }

 	h1 {
		font-size: 60pt;
		color: black;
		margin: 10px 0px 0px 20px;
	}

	.notes {
		display: block;
		font-size: 20pt;
		color: #ff4500;
		margin: 20px 20px 0px 110px;
		padding: 3px;
		width: 300px;
		text-align: center;
	}

	body {
		background: #f1f7f7;
	}

	</style>
</head>
<body>
	<div class="container">
		<h1>Авторизация</h1>
		<form method="post">
			<p><input name="login" type="text" placeholder="Введите логин" required></p>
			<p><input name="pass" type="password" placeholder="Введите пароль" required></p>
			<p><input type="submit" name="autoform"> </p>
		</form>

		<?php 
		include 'connecting.php';		
			if (isset($_POST['autoform']))
			{
				$login=$_POST['login'];
				$pass=$_POST['pass'];
				$res = mysqli_query ($mysqli, "SELECT * FROM users");
				while ($result=mysqli_fetch_assoc($res))
					{
						if ($result['login'] !== $login) {
						$nt = "Логин неверный. Попробуйте снова.";
						}
						elseif ($result['pass'] !== $pass) {
						$nt = "Пароль неверный. Попробуйте снова.";
						}
						else {
							$nt = "Вы авторизированы.";
							break;
						}
					}
				echo '<div class="notes">'.$nt.'</div>';
			}
		?>
	</div>
</body>
</html>